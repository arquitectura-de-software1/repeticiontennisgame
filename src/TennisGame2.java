
public class TennisGame2 implements TennisGame
{
    public int player1Point = 0;
    public int player2Point = 0;
    
    public String player1Result = "";
    public String player2Result = "";
    private String player1Name;
    private String player2Name;

    public TennisGame2(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getLiteralScore(){
        String literalScore = "";
        if (isNormal())
		    literalScore = getLiteral(player1Point) + "-" + getLiteral(player2Point);
		if (isTie())
			literalScore = getLiteral(player1Point) + "-All";
		if (isDeuce())
			literalScore = "Deuce";
		if (isAdvantageOver(player1Point,player2Point))
		    literalScore = "Advantage player1";
		if (isAdvantageOver(player2Point,player1Point))
		    literalScore = "Advantage player2";
		if (isWinnerOver(player1Point,player2Point))
		    literalScore = "Win for player1";
		if (isWinnerOver(player2Point,player1Point))
		    literalScore = "Win for player2";
        return literalScore;  
    }

	private String getLiteral(int playerPoint) {
		String result="";
		if (playerPoint==0)
			result = "Love";
		if (playerPoint==1)
			result = "Fifteen";
		if (playerPoint==2)
			result = "Thirty";
		if (playerPoint==3)
			result = "Forty";
		return result;
	}
	
	private boolean isAdvantageOver(int player1Points, int player2Points) {
		return player1Points > player2Points && player2Points >= 3;
	}
	
	private boolean isWinnerOver(int player1Points, int player2Points) {
		return player1Points>=4 && player2Points>=0 && (player1Points-player2Points)>=2;
	}
	
	private boolean isNormal() {
		return player2Point!= player1Point;
	}

	private boolean isDeuce() {
		return player1Point==player2Point && player1Point>=3;
	}

	private boolean isTie() {
		return player1Point == player2Point && player1Point < 4;
	}
    
    public void SetP1Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P1Score();
        }
            
    }
    
    public void SetP2Score(int number){
        
        for (int i = 0; i < number; i++)
        {
            P2Score();
        }
            
    }
    
    public void P1Score(){
        player1Point++;
    }
    
    public void P2Score(){
        player2Point++;
    }

    public void wonPoint(String player) {
        if (player == "player1")
            P1Score();
        else
            P2Score();
    }
}